(function() {
  function Person(firstName, lastName, age) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
  }

  var theGang = [];
  theGang.push(new Person('Mac', 'McDonald', 36));
  theGang.push(new Person('Dennis', 'Reynolds', 35));
  theGang.push(new Person('Charlie', 'Kelly', 37));
  theGang.push(new Person('Dee', 'Reynolds', 35));
  theGang.push(new Person('Frank', 'Reynolds', 66));

  function getPeopleCount() {
    return (function() {
      return undefined.length;
    })();
  }

  var counter = 0;
  function increment() {
    counter++;
  }

  document.getElementById('log-button').onclick = function() {
    console.log('this is button');
  };

  document.getElementById('warn-button').onclick = function() {
    console.warn('warn');
  };

  document.getElementById('error-button').onclick = function() {
    console.error('error');
  };

  document.getElementById('true-button').onclick = function() {
    console.assert(1 == 1);
  };

  document.getElementById('false-button').onclick = function() {
    console.assert(1 == 2);
  };

  document.getElementById('people-button').onclick = function() {
    console.log(theGang);
  };

  document.getElementById('pretty-people-button').onclick = function() {
    console.table(theGang);
  };

  document.getElementById('exception-button').onclick = function() {
    console.log(getPeopleCount());
  };

  document.getElementById('increment-button').onclick = function() {
    increment();
    document.getElementById('counter').innerText = counter.toString();
  };

  document.getElementById('add-list-item-button').onclick = function() {
    var newChild = document.createElement("li");
    newChild.innerText = "new list item";
    document.getElementById('conditional-breakpoint-list').appendChild(newChild);
  };

  function slowDown() {
    for(var i = 0; i < 1000000000; i++ ) {

    }
  }

  document.getElementById('slow-button').onclick = slowDown;

  var detached;

  function start() {
    var p = document.getElementById("p");
    detached = document.createElement("div");
    p.appendChild(detached);
    p.removeChild(detached);
    fill();
  }

  function fill() {
    for (var i = 0; i < 25; ++i) {
      var div = document.createElement('div');
      div.data = new Array(10000);
      for (var j = 0, l = div.data.length; j < l; ++j)
        div.data[j] = j.toString();
      detached.appendChild(div);
    }
  }

  document.getElementById('leak-button').onclick = start;


})();