# Chrome DevTools Talk Resources
 * [Official docs](https://developers.google.com/chrome-developer-tools/)
 * [Code School course](https://www.codeschool.com/courses/discover-devtools)
 * [Paul Irish talk at Google I/O](https://www.youtube.com/watch?v=x6qe_kVaBpg)
 * [Addy Osmani video](https://www.youtube.com/watch?v=kVSo4buDAEE)
 * [Ilya Grigorik video](https://www.youtube.com/watch?v=BaneWEqNcpE)
